import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-slow',
  templateUrl: './slow.page.html',
  styleUrls: ['./slow.page.scss'],
})
export class SlowPage implements OnInit {

  private cards: any[] = [];

  constructor() { }

  ngOnInit() {
    this.cards = new Array(1000);
  }

}
