import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SlowPage } from './slow.page';

describe('SlowPage', () => {
  let component: SlowPage;
  let fixture: ComponentFixture<SlowPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SlowPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SlowPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
